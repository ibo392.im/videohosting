from django.db import models
from django.utils import timezone
from django.shortcuts import reverse
# Create your models here.


class Category(models.Model):
    title = models.CharField(max_length=290)
    slug = models.SlugField(unique=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def get_absolute_url(self):
        return reverse('category_detail_url', kwargs={'slug': self.slug})


class Member(models.Model):
    slug = models.CharField('Social Network', max_length=255)


class VideoCards(models.Model):
    title = models.CharField(max_length=290)
    slug = models.SlugField(unique=True)
    description = models.TextField(db_index=True)
    image = models.ImageField(default='default-image.png')
    video = models.FileField(db_index=True)
    date = models.DateTimeField(default=timezone.now)
    views = models.IntegerField(default=0)
    category = models.ForeignKey(Category, null=True, blank=True,
                                 on_delete=models.CASCADE, related_name='categories')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Video Card'
        verbose_name_plural = 'Video Cards'

    def get_absolute_url(self):
        return reverse('video_detail_url', kwargs={'slug': self.slug})


class Comment(models.Model):
    video = models.ForeignKey( VideoCards, on_delete=models.CASCADE, related_name='comments')
    author_name = models.CharField(max_length=80)
    comment_text = models.TextField()
    date_published = models.DateField(default=timezone.now)

    def __str__(self):
        return self.comment_text

    class Meta:
        verbose_name = 'Comment'
        verbose_name_plural = 'Comments'


class Like(models.Model):
    video = models.ForeignKey(VideoCards, on_delete=models.CASCADE, null=True)
    comment = models.ForeignKey(Comment, on_delete=models.CASCADE, null=True)
    user_id = models.IntegerField('User', default=0, null=True)
    like = models.BooleanField('Like', default=False)

    def __str__(self):
        return self.video

    class Meta:
        verbose_name = 'Like'
        verbose_name_plural = 'Likes'

class Dislike(models.Model):
    video = models.ForeignKey(VideoCards, on_delete=models.CASCADE, null=True)
    comment = models.ForeignKey(Comment, on_delete=models.CASCADE, null=True)
    user_id = models.IntegerField('User', default=0, null=True)
    dislike = models.BooleanField('Dislike', default=False)

    def __str__(self):
        return self.video

    class Meta:
        verbose_name = 'Dislike'
        verbose_name_plural = 'Dislikes'        
