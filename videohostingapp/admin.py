from django.contrib import admin
from .models import *
# Register your models here.


admin.site.register(Category)
admin.site.register(VideoCards)
admin.site.register(Comment)
admin.site.register(Like)
admin.site.register(Dislike)
admin.site.register(Member)
