from django.shortcuts import render, reverse, redirect
from .models import Category, VideoCards, Member, Comment
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.contrib.auth.models import User 
from django.db.models import Q
from .forms import RegisterForm
# Create your views here.


def homepage(request):
    video1 = VideoCards.objects.order_by('-date')[0:4]
    video2 = VideoCards.objects.order_by('-date')[4:8]
    video3 = VideoCards.objects.order_by('-date')[8:12]
    video4 = VideoCards.objects.order_by('-date')[12:16]
    categories = Category.objects.all()
    member = Member.objects.all()
    return render(request, 'videohostingapp/home.html', {'video1': video1,'video2': video2,'video3': video3,'video4': video4, 'categories': categories, 'member': member})


def video_detail(request, slug):
    video = VideoCards.objects.get(slug__iexact=slug)
    categories = Category.objects.all()
    video.views += 1
    video.save()
    return render(request, 'videohostingapp/video_detail.html', context={'video': video, 'categories': categories})


def search_result(request):
    query = request.GET.get('search')
    categories = Category.objects.all()
    search_obj = VideoCards.objects.filter(
        Q(title__icontains=query) | Q(description__icontains=query))
    return render(request, 'videohostingapp/search.html', {'search_obj': search_obj, 'query': query, 'categories': categories})


def category_detail(request, slug):
    category = Category.objects.get(slug__iexact=slug)
    categories = Category.objects.all()
    return render(request, 'videohostingapp/category_detail.html', context={'category': category, 'categories': categories})


def leave_comment(request, slug):
    try:
        video = VideoCards.objects.get(slug__iexact=slug)
    except:
        raise Http404("Page Not Found")
    if request.user.is_authenticated:
        user = request.user.first_name
        video.comments.create(author_name=user, comment_text=request.POST.get('comment_text'))
    else:
        name = request.POST.get('name')
        video.comments.create(author_name=name, comment_text=request.POST.get('comment_text'))
        # comment = Comment.objects.order_by('-date')[:12]   
    return HttpResponseRedirect(reverse('video_detail_url', args=(video.slug,) ))


def like(request, slug):
    video = VideoCards.objects.get(slug__iexact=slug)
    if request.user.is_authenticated:
        if request.method == 'POST':
            if video.like_set.filter(user_id=request.user.id).exists():
                video.like_set.filter(user_id=request.user.id).delete()
            else:
                video.like_set.create(user_id=request.user.id, like=True)
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def dislike(request, slug):
    video = VideoCards.objects.get(slug__iexact=slug)
    if request.user.is_authenticated:
        if request.method == 'POST':
            if video.dislike_set.filter(user_id=request.user.id).exists():
                video.dislike_set.filter(user_id=request.user.id).delete()
            else:
                video.dislike_set.create(user_id=request.user.id, dislike=True)
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def signup(request):
    categories = Category.objects.all()
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('homepage')
    else:
        form = RegisterForm()
    return render(request, 'videohostingapp/signup.html', {'form': form, 'categories': categories})
