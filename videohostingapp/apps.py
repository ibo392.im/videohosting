from django.apps import AppConfig


class VideohostingappConfig(AppConfig):
    name = 'videohostingapp'
