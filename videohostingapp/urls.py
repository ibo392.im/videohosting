from django.urls import path
from .import views


urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('videos/<slug:slug>/', views.video_detail, name='video_detail_url'),
    path('search/', views.search_result, name='search_result'),
    path('category/<slug:slug>/', views.category_detail,name='category_detail_url'),
    path('videos/<slug:slug>/leave_comment',views.leave_comment, name='leave_comment'),
    path('videos/<slug:slug>/like/', views.like, name='like'),
    path('videos/<slug:slug>/dislike/', views.dislike, name='dislike'),
    path('signup/', views.signup, name="signup"),
]
